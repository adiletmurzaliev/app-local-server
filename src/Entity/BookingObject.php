<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\InheritanceType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookingObjectRepository")
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="discriminator", type="string")
 * @DiscriminatorMap(
 *     {
 *          "BookingObject" = "BookingObject",
 *          "Pension" = "Pension",
 *          "Cottage" = "Cottage"
 *     }
 * )
 * @UniqueEntity("title", message="Такой объект уже существует")
 */
class BookingObject
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=128)
     * @Assert\NotBlank(message="Поле не может быть пустым")
     */
    private $title;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Поле не может быть пустым")
     */
    private $roomsNumber;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=128)
     * @Assert\NotBlank(message="Поле не может быть пустым")
     */
    private $manager;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=128)
     * @Assert\NotBlank(message="Поле не может быть пустым")
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Поле не может быть пустым")
     */
    private $address;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank(message="Поле не может быть пустым")
     */
    private $price;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function setTitle(?string $title): BookingObject
    {
        $this->title = $title;
        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setRoomsNumber(?int $roomsNumber): BookingObject
    {
        $this->roomsNumber = $roomsNumber;
        return $this;
    }

    public function getRoomsNumber(): ?int
    {
        return $this->roomsNumber;
    }

    public function setManager(?string $manager): BookingObject
    {
        $this->manager = $manager;
        return $this;
    }

    public function getManager(): ?string
    {
        return $this->manager;
    }

    public function setPhone(?string $phone): BookingObject
    {
        $this->phone = $phone;
        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setAddress(?string $address): BookingObject
    {
        $this->address = $address;
        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setPrice(?float $price): BookingObject
    {
        $this->price = $price;
        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }
}
