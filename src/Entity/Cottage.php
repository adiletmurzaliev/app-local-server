<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CottageRepository")
 */
class Cottage extends BookingObject
{
    const TYPE = 'Коттедж';

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     * @Assert\NotNull(message="Поле не может быть пустым")
     */
    private $kitchen;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     * @Assert\NotNull(message="Поле не может быть пустым")
     */
    private $shower;


    public function setKitchen(?bool $kitchen): Cottage
    {
        $this->kitchen = $kitchen;
        return $this;
    }

    public function isKitchen(): ?bool
    {
        return $this->kitchen;
    }

    public function setShower(?bool $shower): Cottage
    {
        $this->shower = $shower;
        return $this;
    }

    public function isShower(): ?bool
    {
        return $this->shower;
    }

    public function __toArray() {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'roomsNumber' => $this->getRoomsNumber(),
            'manager' => $this->getManager(),
            'phone' => $this->getPhone(),
            'address' => $this->getAddress(),
            'price' => $this->getPrice(),
            'kitchen' => $this->isKitchen(),
            'shower' => $this->isShower(),
            'type' => self::TYPE
        ];
    }
}
