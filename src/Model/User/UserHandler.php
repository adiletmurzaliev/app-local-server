<?php

namespace App\Model\User;

use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class UserHandler
{
    const SOC_NETWORK_VKONTAKTE = "vkontakte";
    const SOC_NETWORK_FACEBOOK = "facebook";
    const SOC_NETWORK_GOOGLE = "google";

    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var ApiContext
     */
    private $apiContext;

    public function __construct(ContainerInterface $container, ApiContext $apiContext)
    {
        $this->container = $container;
        $this->apiContext = $apiContext;
    }

    public function createUser(array $data, bool $encodePassword = true): User
    {
        $user = new User();
        $user
            ->setEmail($data['email'])
            ->setPassport($data['passport'])
            ->setVkId($data['vkId'] ?? null)
            ->setFaceBookId($data['faceBookId'] ?? null)
            ->setGoogleId($data['googleId'] ?? null)
            ->setRoles($data['roles'] ?? ['ROLE_USER']);

        if ($encodePassword) {
            $password = $this->encodePassword($data['password']);
        } else {
            $password = $data['password'];
        }

        $user->setPassword($password);

        return $user;
    }

    public function makeUserSession(User $user)
    {
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this
            ->container
            ->get('security.token_storage')
            ->setToken($token);
        $this
            ->container
            ->get('session')
            ->set('_security_main', serialize($token));
    }

    public function encodePassword(string $password): string
    {
        try {
            return $this->apiContext->encodePassword($password);
        } catch (ApiException $e) {
            throw new HttpException(
                $e->getCode(),
                "Ошибка {$e->getCode()}: {$e->getMessage()} (Ошибка API сервера)"
            );
        }
    }

    public function isPasswordValid(string $plainPassword, string $encodedPassword): bool
    {
        return password_verify($plainPassword, $encodedPassword);
    }
}
