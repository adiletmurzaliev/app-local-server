<?php

namespace App\Model\Api;

class ApiContext extends AbstractApiContext
{
    const ENDPOINT_PING = '/ping';
    const ENDPOINT_CLIENT_CREATE = '/client/create';
    const ENDPOINT_CLIENT_EXISTS = '/client/exists/{passport}/{email}';
    const ENDPOINT_CLIENT_GET_DATA = '/client/get-data/{email}';
    const ENDPOINT_CLIENT_CHECK_CREDENTIALS = '/client/check-credentials/{email}/{plainPassword}';
    const ENDPOINT_CLIENT_PASSWORD_ENCODE = '/client/password-encode';
    const ENDPOINT_CLIENT_CONNECT_SOCIAL = '/client/connect-social';
    const ENDPOINT_CLIENT_EXISTS_BY_SOCIAL = '/client/exists-by-social/{network}/{uid}';
    const ENDPOINT_BOOKING_OBJECT_EXISTS = '/booking-object/exists/{title}';
    const ENDPOINT_BOOKING_OBJECT_CREATE_PENSION = '/booking-object/create-pension';
    const ENDPOINT_BOOKING_OBJECT_CREATE_COTTAGE = '/booking-object/create-cottage';
    const ENDPOINT_BOOKING_OBJECT_GET_ALL = '/booking-object/get-all';
    const ENDPOINT_BOOKING_OBJECT_GET_ALL_FILTER = '/booking-object/get-all-filter';
    const ENDPOINT_BOOKING_OBJECT_GET_DATA = '/booking-object/get-data/{title}';
    const ENDPOINT_BOOKING_OBJECT_BOOK = '/booking-object/book';

    /**
     * @return mixed
     * @throws ApiException
     */
    public function makePing()
    {
        return $this->makeQuery(self::ENDPOINT_PING, self::METHOD_GET);
    }

    /**
     * @param string $passport
     * @param string $email
     * @return mixed
     * @throws ApiException
     */
    public function clientExists(string $passport, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CLIENT_EXISTS, [
            'passport' => $passport,
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createClient(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CLIENT_CREATE, self::METHOD_POST, $data);
    }

    /**
     * @param string $email
     * @return mixed
     * @throws ApiException
     */
    public function getClientData(string $email)
    {

        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CLIENT_GET_DATA, [
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @param string $email
     * @param string $plainPassword
     * @return mixed
     * @throws ApiException
     */
    public function checkClientCredentials(string $email, string $plainPassword)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CLIENT_CHECK_CREDENTIALS, [
            'email' => $email,
            'plainPassword' => $plainPassword
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param string $password
     * @return mixed
     * @throws ApiException
     */
    public function encodePassword(string $password)
    {
        $response = $this->makeQuery(self::ENDPOINT_CLIENT_PASSWORD_ENCODE, self::METHOD_GET, [
            'plainPassword' => $password
        ]);

        return $response['result'];
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function connectSocial(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CLIENT_CONNECT_SOCIAL, self::METHOD_POST, $data);
    }

    /**
     * @param string $network
     * @param string $uid
     * @return mixed
     * @throws ApiException
     */
    public function clientExistsBySocial(string $network, string $uid)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CLIENT_EXISTS_BY_SOCIAL, [
            'network' => $network,
            'uid' => $uid
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param string $title
     * @return mixed
     * @throws ApiException
     */
    public function bookingObjectExists(string $title)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_BOOKING_OBJECT_EXISTS, [
            'title' => $title
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createPension(array $data)
    {
        return $this->makeQuery(
            self::ENDPOINT_BOOKING_OBJECT_CREATE_PENSION,
            self::METHOD_POST,
            $data
        );
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createCottage(array $data)
    {
        return $this->makeQuery(
            self::ENDPOINT_BOOKING_OBJECT_CREATE_COTTAGE,
            self::METHOD_POST,
            $data
        );
    }

    /**
     * @return mixed
     * @throws ApiException
     */
    public function getAllBookingObjects()
    {
        return $this->makeQuery(self::ENDPOINT_BOOKING_OBJECT_GET_ALL, self::METHOD_GET);
    }

    /**
     * @param array $query
     * @return mixed
     * @throws ApiException
     */
    public function getAllBookingObjectsByFilter(array $query)
    {
        return $this->makeQuery(
            self::ENDPOINT_BOOKING_OBJECT_GET_ALL_FILTER,
            self::METHOD_GET,
            $query
        );
    }

    /**
     * @param string $title
     * @return mixed
     * @throws ApiException
     */
    public function getBookingObjectData(string $title)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_BOOKING_OBJECT_GET_DATA, [
            'title' => $title
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function book(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_BOOKING_OBJECT_BOOK, self::METHOD_PATCH, $data);
    }
}