<?php

namespace App\Repository;

use App\Entity\BookingObject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BookingObject|null find($id, $lockMode = null, $lockVersion = null)
 * @method BookingObject|null findOneBy(array $criteria, array $orderBy = null)
 * @method BookingObject[]    findAll()
 * @method BookingObject[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingObjectRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BookingObject::class);
    }

//    /**
//     * @return BookingObject[] Returns an array of BookingObject objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BookingObject
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
