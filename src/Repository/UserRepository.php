<?php

namespace App\Repository;

use App\Entity\User;
use App\Model\User\UserHandler;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param string $email
     * @return User|null
     */
    public function findOneByUsername(string $email): ?User
    {
        try {
            return $this->createQueryBuilder('u')
                ->select('u')
                ->where('u.email = :email')
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param string $network
     * @param string $uid
     * @return User|null
     */
    public function findBySocialId(string $network, string $uid): ?User
    {
        try {
            $result = $this->createQueryBuilder('u')->select('u');

            switch ($network) {
                case UserHandler::SOC_NETWORK_VKONTAKTE:
                    $result->where('u.vkId = :uid');
                    break;
                case UserHandler::SOC_NETWORK_FACEBOOK:
                    $result->where('u.faceBookId = :uid');
                    break;
                case UserHandler::SOC_NETWORK_GOOGLE:
                    $result->where('u.googleId = :uid');
                    break;
            }

            return $result
                ->setParameter('uid', $uid)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();

        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
