<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\BookingFilterType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/", name="app_index")
     *
     * @param ApiContext $apiContext
     * @return Response
     */
    public function index(ApiContext $apiContext): Response
    {
        $form = $this->createForm(BookingFilterType::class, null, [
            'action' => $this->generateUrl('app_filter')
        ]);

        try {
            $bookingObjects = $apiContext->getAllBookingObjects();
        } catch (ApiException $e) {
            throw new HttpException(
                $e->getCode(),
                "Ошибка {$e->getCode()}: {$e->getMessage()} (Ошибка API сервера)"
            );
        }

        return $this->render('index/index.html.twig', [
            'booking_objects' => $bookingObjects,
            'form' => $form->createView(),
            'nothing_found' => false
        ]);
    }

    /**
     * @Route("/filter", name="app_filter")
     * @Method("POST")
     *
     * @param Request $request
     * @param ApiContext $apiContext
     * @return Response
     */
    public function filter(Request $request, ApiContext $apiContext): Response
    {
        $nothingFound = false;

        $form = $this->createForm(BookingFilterType::class);
        $form->handleRequest($request);
        $filterData = $form->getData();

        try {
            $bookingObjects = $apiContext->getAllBookingObjectsByFilter($filterData);
        } catch (ApiException $e) {
            throw new HttpException(
                $e->getCode(),
                "Ошибка {$e->getCode()}: {$e->getMessage()} (Ошибка API сервера)"
            );
        }

        if (empty($bookingObjects)) {
            $nothingFound = true;
        }

        return $this->render('index/index.html.twig', [
            'booking_objects' => $bookingObjects,
            'form' => $form->createView(),
            'nothing_found' => $nothingFound
        ]);
    }

    /**
     * @Route("/show/{title}", name="app_show")
     * @Method({"GET", "POST"})
     *
     * @param ApiContext $apiContext
     * @param string $title
     * @return Response
     * @throws \Exception
     */
    public function show(ApiContext $apiContext, string $title): Response
    {
        try {
            $booking = $apiContext->getBookingObjectData($title);
        } catch (ApiException $e) {
            throw new HttpException(
                $e->getCode(),
                "Ошибка {$e->getCode()}: {$e->getMessage()} (Ошибка API сервера)"
            );
        }

        if (!$booking) {
            return $this->redirectToRoute('app_index');
        }

        foreach ($booking['rooms'] as $key => $room) {
            $rentDate = new \DateTime($room['rentDate']['date']);
            $currentDate = new \DateTime();
            $booking['rooms'][$key]['rentDate'] = $rentDate;
            $booking['rooms'][$key]['isBooked'] = true;

            if ($rentDate < $currentDate) {
                $booking['rooms'][$key]['isBooked'] = false;
                $booking['rooms'][$key]['shortBook'] = new \DateTime('+7 day');
                $booking['rooms'][$key]['longBook'] = new \DateTime('+14 day');
            }
        }

        return $this->render('index/show.html.twig', [
            'booking' => $booking
        ]);
    }

    /**
     * @Route("/book/{title}/{room}/{date}", name="app_book")
     * @Route("PATCH")
     *
     * @param string $title
     * @param string $room
     * @param string $date
     * @param ApiContext $apiContext
     * @return Response
     */
    public function book(string $title, string $room, string $date, ApiContext $apiContext): Response
    {
        if ($this->isGranted('ROLE_LANDLORD')) {
            $this->addFlash(
                'danger',
                'Арендодатель не может бронировать.'
            );

            return $this->redirectToRoute('app_show', ['title' => $title]);
        }

        if ($this->isGranted('ROLE_TENANT')) {

            /** @var User $user */
            $user = $this->getUser();

            try {
                $data = [
                    'title' => $title,
                    'roomNumber' => $room,
                    'rentDate' => $date,
                    'clientPassport' => $user->getPassport(),
                    'clientEmail' => $user->getEmail(),
                ];

                $apiContext->book($data);

            } catch (ApiException $e) {
                throw new HttpException(
                    $e->getCode(),
                    "Ошибка {$e->getCode()}: {$e->getMessage()} (Ошибка API сервера)"
                );
            }

            return $this->redirectToRoute('app_show', ['title' => $title]);
        }

        if ($this->isGranted('IS_AUTHENTICATED_ANONYMOUSLY')) {
            $this->addFlash(
                'danger',
                'Пожалуйста авторизуйтесь, чтобы бронировать.'
            );

            return $this->redirectToRoute('app_show', ['title' => $title]);
        }
    }
}
