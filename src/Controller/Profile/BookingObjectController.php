<?php

namespace App\Controller\Profile;

use App\Entity\BookingObject;
use App\Entity\Cottage;
use App\Entity\Pension;
use App\Form\BookingType;
use App\Form\CottageType;
use App\Form\PensionType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/profile")
 *
 * Class ProfileController
 * @package App\Controller
 */
class BookingObjectController extends Controller
{
    /**
     * @Route("/create-booking", name="profile_create_booking")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param ApiContext $apiContext
     * @return Response
     */
    public function createBooking(Request $request, ApiContext $apiContext): Response
    {
        $bookingObject = new BookingObject();

        $form = $this->createForm(BookingType::class, $bookingObject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {
                if ($apiContext->bookingObjectExists($bookingObject->getTitle())) {
                    $form->get('title')->addError(new FormError('Такой объект уже существует'));
                } else {
                    $bookingType = $form->get('booking_type')->getData();
                    switch ($bookingType) {

                        case Pension::TYPE:

                            $pension = new Pension();
                            $this->fillBookingData($pension, $bookingObject);
                            $form = $this->createForm(PensionType::class, $pension, [
                                'action' => $this->generateUrl('profile_create_pension')
                            ]);

                            return $this->render('profile/booking_object/create_pension.html.twig', [
                                'form' => $form->createView()
                            ]);

                        case Cottage::TYPE:

                            $cottage = new Cottage();
                            $this->fillBookingData($cottage, $bookingObject);
                            $form = $this->createForm(CottageType::class, $cottage, [
                                'action' => $this->generateUrl('profile_create_cottage')
                            ]);

                            return $this->render('profile/booking_object/create_cottage.html.twig', [
                                'form' => $form->createView()
                            ]);
                    }
                }
            } catch (ApiException $e) {
                throw new HttpException(
                    $e->getCode(),
                    "Ошибка {$e->getCode()}: {$e->getMessage()} (Ошибка API сервера)"
                );
            }
        }

        return $this->render('profile/booking_object/create_booking.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/create-pension", name="profile_create_pension")
     * @Method("POST")
     *
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param ApiContext $apiContext
     * @return Response
     */
    public function createPension(Request $request, EntityManagerInterface $manager, ApiContext $apiContext): Response
    {
        $pension = new Pension();

        $form = $this->createForm(PensionType::class, $pension);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $pension->__toArray();

            try {
                $apiContext->createPension($data);
            } catch (ApiException $e) {
                throw new HttpException(
                    $e->getCode(),
                    "Ошибка {$e->getCode()}: {$e->getMessage()} (Ошибка API сервера)"
                );
            }

            $manager->persist($pension);
            $manager->flush();

            return $this->redirectToRoute('app_index');
        }

        return $this->render('profile/booking_object/create_pension.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/create-cottage", name="profile_create_cottage")
     * @Method("POST")
     *
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param ApiContext $apiContext
     * @return Response
     */
    public function createCottage(Request $request, EntityManagerInterface $manager, ApiContext $apiContext): Response
    {
        $cottage = new Cottage();

        $form = $this->createForm(CottageType::class, $cottage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $cottage->__toArray();

            try {
                $apiContext->createCottage($data);
            } catch (ApiException $e) {
                throw new HttpException(
                    $e->getCode(),
                    "Ошибка {$e->getCode()}: {$e->getMessage()} (Ошибка API сервера)"
                );
            }

            $manager->persist($cottage);
            $manager->flush();

            return $this->redirectToRoute('app_index');
        }

        return $this->render('profile/booking_object/create_cottage.html.twig', [
            'form' => $form->createView()
        ]);
    }


    private function fillBookingData(BookingObject $object, BookingObject $bookingObject)
    {
        $object
            ->setTitle($bookingObject->getTitle())
            ->setRoomsNumber($bookingObject->getRoomsNumber())
            ->setManager($bookingObject->getManager())
            ->setAddress($bookingObject->getAddress())
            ->setPhone($bookingObject->getPhone())
            ->setPrice($bookingObject->getPrice());
    }

}