<?php

namespace App\Controller\Profile;

use App\Entity\BookingObject;
use App\Entity\Cottage;
use App\Entity\Pension;
use App\Form\BookingType;
use App\Form\CottageType;
use App\Form\PensionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/profile")
 *
 * Class ProfileController
 * @package App\Controller
 */
class ProfileController extends Controller
{
    /**
     * @Route("/", name="profile_index")
     * @Method("GET")
     *
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('profile/index.html.twig', [

        ]);
    }
}