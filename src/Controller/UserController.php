<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\LoginType;
use App\Form\ULoginRegisterType;
use App\Form\UserType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/user")
 *
 * Class UserController
 * @package App\Controller
 */
class UserController extends Controller
{
    const ULOGIN_DATA = 'ulogin-data';

    /**
     * @Route("/register", name="user_register")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param UserHandler $userHandler
     * @param EntityManagerInterface $em
     * @param ApiContext $apiContext
     * @return Response
     */
    public function register(
        Request $request,
        UserHandler $userHandler,
        EntityManagerInterface $em,
        ApiContext $apiContext
    ): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {

                    $form->get('passport')->addError(new FormError('Пользователь с такими данными уже зарегестрирован'));
                    $form->get('email')->addError(new FormError('Такой email уже используется'));
                    return $this->render('user/register.html.twig', [
                        'form' => $form->createView()
                    ]);

                } else {

                    $user->setPassword($userHandler->encodePassword($user->getPlainPassword()));

                    $data = $user->__toArray();
                    $apiContext->createClient($data);

                    $user = $userHandler->createUser($data, false);
                    $em->persist($user);
                    $em->flush();

                    return $this->render('user/success.html.twig');

                }
            } catch (ApiException $e) {
                throw new HttpException(
                    $e->getCode(),
                    "Ошибка {$e->getCode()}: {$e->getMessage()} (Ошибка API сервера)"
                );
            }
        }

        return $this->render('user/register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/login", name="user_login")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $em
     * @param ApiContext $apiContext
     * @param UserHandler $userHandler
     * @return Response
     */
    public function login(
        Request $request,
        UserRepository $userRepository,
        EntityManagerInterface $em,
        ApiContext $apiContext,
        UserHandler $userHandler
    ): Response
    {
        $error = false;

        $form = $this->createForm(LoginType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $user = $userRepository->findOneByUsername($data['email']);
            $error = 'Введены неправильные данные';

            if ($user) {
                if ($userHandler->isPasswordValid($data['plainPassword'], $user->getPassword())) {
                    $userHandler->makeUserSession($user);
                    return $this->redirectToRoute('profile_index');
                }
            } else {

                try {
                    if ($apiContext->checkClientCredentials($data['email'], $data['plainPassword'])) {
                        $data = $apiContext->getClientData($data['email']);
                        $data['registeredAt'] = new \DateTime($data['registeredAt']['date']);
                        $user = $userHandler->createUser($data, false);

                        $em->persist($user);
                        $em->flush();
                        $userHandler->makeUserSession($user);

                        return $this->redirectToRoute('profile_index');
                    } else {
                        $error = 'Вы еще не регистрировались у нас';
                    }
                } catch (ApiException $e) {
                    throw new HttpException(
                        $e->getCode(),
                        "Ошибка {$e->getCode()}: {$e->getMessage()} (Ошибка API сервера)"
                    );
                }
            }
        }

        return $this->render('user/login.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/register-social-start", name="user_register_social_start")
     * @Method({"GET", "POST"})
     *
     * @return Response
     */
    public function registerSocialStart(): Response
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);

        $user = new User();
        $user->setEmail($userData['email']);

        $form = $this->createForm(ULoginRegisterType::class, $user, [
            'action' => $this->generateUrl('user_register_social_end')
        ]);

        $this->get('session')->set(self::ULOGIN_DATA, $s);

        return $this->render('user/ulogin_register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/register-social-end", name="user_register_social_end")
     * @Method("POST")
     *
     * @param Request $request
     * @param ApiContext $apiContext
     * @param UserHandler $userHandler
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function registerSocialEnd(
        Request $request,
        ApiContext $apiContext,
        UserHandler $userHandler,
        EntityManagerInterface $manager
    ): Response
    {
        $userData = json_decode($this->get('session')->get(self::ULOGIN_DATA), true);

        $user = new User();

        $form = $this->createForm(ULoginRegisterType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {

                    $form->get('passport')->addError(new FormError('Пользователь с такими данными уже зарегестрирован'));
                    $form->get('email')->addError(new FormError('Такой email уже используется'));
                    return $this->render('user/ulogin_register.html.twig', [
                        'form' => $form->createView()
                    ]);

                } else {

                    $user->setPassword($userHandler->encodePassword("social------------------------" . time()));
                    $user->setSocialId($userData['network'], $userData['uid']);
                    $data = $user->__toArray();
                    $apiContext->createClient($data);
                    $user = $userHandler->createUser($data, false);
                    $manager->persist($user);
                    $manager->flush();

                    return $this->render('user/success.html.twig');

                }
            } catch (ApiException $e) {
                throw new HttpException(
                    $e->getCode(),
                    "Ошибка {$e->getCode()}: {$e->getMessage()} (Ошибка API сервера)"
                );
            }
        }

        return $this->render('user/ulogin_register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/connect-social", name="user_connect_social")
     * @Method({"GET", "POST"})
     *
     * @param EntityManagerInterface $manager
     * @param ApiContext $apiContext
     * @return Response
     */
    public function connectSocial(EntityManagerInterface $manager, ApiContext $apiContext): Response
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);

        /** @var User $user */
        $user = $this->getUser();
        $user->setSocialId($userData['network'], $userData['uid']);

        $data = [
            'email' => $user->getEmail(),
            'passport' => $user->getPassport(),
            'network' => $userData['network'],
            'uid' => $userData['uid']
        ];

        try {
            if ($apiContext->connectSocial($data)) {
                $manager->persist($user);
                $manager->flush();
            }
        } catch (ApiException $e) {
            throw new HttpException(
                $e->getCode(),
                "Ошибка {$e->getCode()}: {$e->getMessage()} (Ошибка API сервера)"
            );
        }

        return $this->redirectToRoute('profile_index');
    }

    /**
     * @Route("/login-social", name="user_login_social")
     *
     * @param UserRepository $userRepository
     * @param UserHandler $userHandler
     * @param ApiContext $apiContext
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function loginSocial(
        UserRepository $userRepository,
        UserHandler $userHandler,
        ApiContext $apiContext,
        EntityManagerInterface $manager
    ): Response
    {
        $error = false;
        $form = $this->createForm(LoginType::class);

        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);

        $user = $userRepository->findBySocialId($userData['network'], $userData['uid']);

        if ($user) {
            $userHandler->makeUserSession($user);
            return $this->redirectToRoute('profile_index');
        } else {

            try {
                if ($apiContext->clientExistsBySocial($userData['network'], $userData['uid'])) {
                    $data = $apiContext->getClientData($userData['email']);
                    $data['registeredAt'] = new \DateTime($data['registeredAt']['date']);
                    $user = $userHandler->createUser($data, false);

                    $manager->persist($user);
                    $manager->flush();
                    $userHandler->makeUserSession($user);

                    return $this->redirectToRoute('profile_index');
                } else {
                    $error = 'Вы еще не регистрировались у нас, либо не привязали данную соц сеть к аккаунту';
                }
            } catch (ApiException $e) {
                throw new HttpException(
                    $e->getCode(),
                    "Ошибка {$e->getCode()}: {$e->getMessage()} (Ошибка API сервера)"
                );
            }
        }

        return $this->render('user/login.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }
}
