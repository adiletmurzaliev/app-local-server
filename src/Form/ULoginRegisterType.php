<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ULoginRegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, ['label' => false])
            ->add('passport', TextType::class, ['label' => false])
            ->add('submit', SubmitType::class, ['label' => 'Регистрация'])
            ->add('roles', ChoiceType::class, [
                'label' => 'Кем Вы являетесь:',
                'choices' => [
                    'Арендатор' => 'tenant',
                    'Арендодатель' => 'landlord'
                ]
            ]);

        /** @var User $user */
        $user = $builder->getData();

        $builder->get('roles')
            ->addModelTransformer(new CallbackTransformer(
                function ($storedRoles) use (&$user) {
                    $currentRoles = $user->getRoles();
                    if (in_array('ROLE_TENANT', $currentRoles)) {
                        return 'tenant';
                    } elseif (in_array('ROLE_LANDLORD', $currentRoles)) {
                        return 'landlord';
                    } else {
                        return null;
                    }
                },
                function ($forViewOfRole) use (&$user) {
                    $currentRoles = array_diff($user->getRoles(), [
                        'ROLE_TENANT',
                        'ROLE_LANDLORD',
                    ]);

                    switch ($forViewOfRole) {
                        case 'tenant':
                            $currentRoles[] = 'ROLE_TENANT';
                            break;
                        case 'landlord':
                            $currentRoles[] = 'ROLE_LANDLORD';
                            break;
                        default:
                            $currentRoles = $user->getRoles();
                            break;
                    }

                    return $currentRoles;
                }
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
