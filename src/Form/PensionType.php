<?php

namespace App\Form;

use App\Entity\Pension;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PensionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', HiddenType::class)
            ->add('roomsNumber', HiddenType::class)
            ->add('manager', HiddenType::class)
            ->add('phone', HiddenType::class)
            ->add('address', HiddenType::class)
            ->add('price', HiddenType::class)
            ->add('pool', ChoiceType::class, [
                'label' => 'Бассейн',
                'choices' => [
                    'Есть' => true,
                    'Нету' => false
                ]
            ])
            ->add('cinema', ChoiceType::class, [
                'label' => 'Кинотеатр',
                'choices' => [
                    'Есть' => true,
                    'Нету' => false
                ]
            ])
            ->add('description', TextareaType::class, ['label' => false])
            ->add('submit', SubmitType::class, ['label' => 'Регистрация'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Pension::class,
        ]);
    }
}
