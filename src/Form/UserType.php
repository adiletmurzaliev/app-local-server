<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, ['label' => false])
            ->add('passport', TextType::class, ['label' => false])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'required' => true,
                'first_options' => ['label' => false],
                'second_options' => ['label' => false],
                'invalid_message' => 'Пароли должны совпадать',
                'constraints' => new NotBlank(['message' => 'Пароль не может быть пустым'])
            ])
            ->add('roles', ChoiceType::class, [
                'label' => 'Кем Вы являетесь:',
                'constraints' => new NotBlank(['message' => 'Выберите тип пользователя']),
                'choices' => [
                    'Арендатор' => 'tenant',
                    'Арендодатель' => 'landlord'
                ],
            ])
            ->add('submit', SubmitType::class, ['label' => 'Регистрация']);

        /** @var User $user */
        $user = $builder->getData();

        $builder->get('roles')
            ->addModelTransformer(new CallbackTransformer(
                function ($storedRoles) use (&$user) {
                    $currentRoles = $user->getRoles();
                    if (in_array('ROLE_TENANT', $currentRoles)) {
                        return 'tenant';
                    } elseif (in_array('ROLE_LANDLORD', $currentRoles)) {
                        return 'landlord';
                    } else {
                        return null;
                    }
                },
                function ($forViewOfRole) use (&$user) {
                    $currentRoles = array_diff($user->getRoles(), [
                        'ROLE_TENANT',
                        'ROLE_LANDLORD',
                    ]);

                    switch ($forViewOfRole) {
                        case 'tenant':
                            $currentRoles[] = 'ROLE_TENANT';
                            break;
                        case 'landlord':
                            $currentRoles[] = 'ROLE_LANDLORD';
                            break;
                        default:
                            $currentRoles = $user->getRoles();
                            break;
                    }

                    return $currentRoles;
                }
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
