<?php

namespace App\Form;

use App\Entity\Cottage;
use App\Entity\Pension;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookingFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, ['label' => false])
            ->add('minPrice', IntegerType::class, ['label' => false])
            ->add('maxPrice', IntegerType::class, ['label' => false])
            ->add('booking_type', ChoiceType::class, [
                'label' => false,
                'choices' => [
                    'Все' => null,
                    'Пансионат' => Pension::TYPE,
                    'Коттедж' => Cottage::TYPE
                ]
            ])
            ->add('pension_choices', ChoiceType::class, [
                'label' => false,
                'expanded' => true,
                'multiple' => true,
                'choices' => [
                    'Бассейн' => 'pool',
                    'Кинотеатр' => 'cinema'
                ]
            ])
            ->add('cottage_choices', ChoiceType::class, [
                'label' => false,
                'expanded' => true,
                'multiple' => true,
                'choices' => [
                    'Кухня' => 'kitchen',
                    'Душ' => 'shower'
                ]
            ])
            ->add('submit', SubmitType::class, ['label' => 'Поиск'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
