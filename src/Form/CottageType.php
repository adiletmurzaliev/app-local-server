<?php

namespace App\Form;

use App\Entity\Cottage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CottageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', HiddenType::class)
            ->add('roomsNumber', HiddenType::class)
            ->add('manager', HiddenType::class)
            ->add('phone', HiddenType::class)
            ->add('address', HiddenType::class)
            ->add('price', HiddenType::class)
            ->add('kitchen', ChoiceType::class, [
                'label' => 'Кухня',
                'choices' => [
                    'Есть' => true,
                    'Нету' => false
                ]
            ])
            ->add('shower', ChoiceType::class, [
                'label' => 'Душ',
                'choices' => [
                    'Есть' => true,
                    'Нету' => false
                ]
            ])
            ->add('submit', SubmitType::class, ['label' => 'Регистрация'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cottage::class,
        ]);
    }
}
