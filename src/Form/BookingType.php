<?php

namespace App\Form;

use App\Entity\BookingObject;
use App\Entity\Cottage;
use App\Entity\Pension;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('booking_type', ChoiceType::class, [
                'mapped' => false,
                'label' => 'Тип объекта',
                'choices' => [
                    'Пансионат' => Pension::TYPE,
                    'Коттедж' => Cottage::TYPE
                ]
            ])
            ->add('title', TextType::class, ['label' => false])
            ->add('roomsNumber', IntegerType::class, ['label' => false])
            ->add('manager', TextType::class, ['label' => false])
            ->add('phone', TelType::class, ['label' => false])
//            ->add('address', TextType::class, [
//                'label' => false,
//                'disabled' => true
//            ])
            ->add('address', HiddenType::class)
            ->add('price', NumberType::class, ['label' => false])
            ->add('submit', SubmitType::class, ['label' => 'Далее'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BookingObject::class,
        ]);
    }
}
