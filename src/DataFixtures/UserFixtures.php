<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/13/18
 * Time: 7:39 PM
 */

namespace App\DataFixtures;

use App\Entity\User;
use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    /** @var UserHandler */
    private $userHandler;

    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    public function load(ObjectManager $manager)
    {
        $user = $this->userHandler->createUser([
            'email' => 'client1@mail.com',
            'passport' => 'AHclient1',
            'password' => password_hash('123', PASSWORD_BCRYPT),
            'roles' => ['ROLE_USER', 'ROLE_TENANT']
        ], false);

        $manager->persist($user);
        $manager->flush();

        $user = $this->userHandler->createUser([
            'email' => 'client2@mail.com',
            'passport' => 'AHclient2',
            'password' => password_hash('123', PASSWORD_BCRYPT),
            'roles' => ['ROLE_USER', 'ROLE_LANDLORD']
        ], false);

        $manager->persist($user);
        $manager->flush();
    }
}
