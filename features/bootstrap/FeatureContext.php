<?php

use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends AttractorContext
{
    /**
     * @When /^я вижу слово "([^"]*)" где\-то на странице$/
     */
    public function iSeeWordOnPage($arg1)
    {
        $this->assertPageContainsText($arg1);
    }

    /**
     * @When /^я не вижу слово "([^"]*)" где\-то на странице$/
     */
    public function iDontSeeWordOnPage($arg1)
    {
        $this->assertPageNotContainsText($arg1);
    }

    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function iOnHomepage()
    {
        $this->visit($this->getContainer()->get('router')->generate('app_index'));
    }

    /**
     * @When /^я перехожу по маршруту "([^"]*)"$/
     */
    public function visitLink($arg1)
    {
        $this->visit($this->getContainer()->get('router')->generate($arg1));
    }

    /**
     * @When /^я заполняю поле формы: ([^"]*), значением: ([^"]*)$/
     * @param $field
     * @param $value
     */
    public function iFillInputFiledWithValue($field, $value)
    {
        $this->fillField($field, $value);
    }

    /**
     * @When /^я нажимаю на кнопку: ([^"]*)$/
     * @param $button
     */
    public function iPressOnButton($button)
    {
        $this->pressButton($button);
    }

    /**
     * @When /^я нажимаю на ссылку: ([^"]*)$/
     * @param $link
     */
    public function iPressOnLink($link)
    {
        try {
            $this->getSession()->getPage()->clickLink($link);
        } catch (\Behat\Mink\Exception\ElementNotFoundException $e) {
            throw new \LogicException('Нету');
        }
    }

    /**
     * @When /^я захожу на сайт как "([^"]*)" с паролем "([^"]*)"$/
     * @param $username
     * @param $password
     */
    public function iLoginAs($username, $password)
    {
        $this->visit($this->getContainer()->get('router')->generate('user_login'));
        $this->fillField('login[email]', $username);
        $this->fillField('login[plainPassword]', $password);
        $this->pressButton('Войти');
        $this->iSeeLink('Личный кабинет');
    }

    /**
     * @When /^я вижу кнопку: ([^"]*)$/
     * @param $button
     */
    public function iSeeButton($button)
    {
        if (!$this->getSession()->getPage()->hasButton($button)) {
            throw new \LogicException('Нету');
        }
    }

    /**
     * @When /^я вижу ссылку: ([^"]*)$/
     * @param $link
     */
    public function iSeeLink($link)
    {
        if (!$this->getSession()->getPage()->hasLink($link)) {
            throw new \LogicException('Нету');
        }
    }

    /**
     * @When /^я бронирую первую свободную комнату$/
     */
    public function iBook()
    {
        $element = $this->getSession()->getPage()->find('css', '.list-group-item .btn');

        if ($element === null) {
            throw new \LogicException('Нету');
        }

        $element->click();
    }

    /**
     * @When /^я жду: ([^"]*) сек.$/
     * @param $seconds
     */
    public function iWaitFor($seconds)
    {
        $this->getSession()->wait($seconds * 1000);
    }

    /**
     * @When /^я кликаю на карту$/
     */
    public function iClickOnMap()
    {
        $element = $this->getSession()->getPage()->find('css', '#map');

        if ($element === null) {
            throw new \LogicException('Нету');
        }

        $element->click();
    }

    /**
     * @When /^я выбираю: ([^"]*) со значением: ([^"]*)$/
     * @param $select
     * @param $option
     */
    public function iSelectOption($select, $option)
    {
        $this->selectOption($select, $option);
    }

    /**
     * @When /^я отмечаю галочкой опцию: ([^"]*)$/
     * @param $option
     */
    public function iCheckOption($option)
    {
        $this->checkOption($option);
    }
}

